const bodyParser = require('koa-bodyparser');
const Koa = require('koa');
const cors = require("@koa/cors");
const app = new Koa();
app.use(bodyParser());
// Import required AWS SDK clients and commands for Node.js
const { TextractClient, AnalyzeDocumentCommand } = require("@aws-sdk/client-textract");

var options = {
  origin: '*',
  allowMethods: ["POST", "OPTIONS"],
  allowHeaders: '*'
};

app.use(cors(options));

// Set the AWS Region.
const REGION = "eu-west-1";
// Create SNS service object.
const textractClient = new TextractClient({
  region: REGION,
  aws_access_key_id: process.env.AWS_ACCESS_KEY_ID,
  aws_secret_access_key: process.env.AWS_SECRET_ACCESS_KEY,
});

// Set params
const params = {
  // Bytes: <base64 data>
  FeatureTypes: ['TABLES', 'FORMS'],
}

const findMatches = async (response, query, ConfidenceMinimum) => {
  try {
      const results = Object.values(response.Blocks).reduce((matches, block) => {
          if ("Text" in block && block.Text !== undefined){
              if (block.Confidence > ConfidenceMinimum && `${block.Text}`.toLowerCase().includes(query.toLowerCase())) {
                console.log(`Text: ${block.Text} includes ${query}`);
                matches.push(block);
              }
          }
          return matches;
      }, []);
      return results;
    } catch (err) {
      console.log("Error", err);
    }
}

const analyze_document_text = async (analyzeParams, query, ConfidenceMinimum) => {
  try {
      const analyzeDoc = new AnalyzeDocumentCommand(analyzeParams);
      const response = await textractClient.send(analyzeDoc);
      const matches = await findMatches(response, query, ConfidenceMinimum);
      return { matches, queryFound: matches.length > 0 }; // For unit tests.
    } catch (err) {
      console.log("Error", err);
    }
}

app.on('error', err => {
  console.error('server error', err)
});

app.use(async ctx => {
  console.log('RECIEVED REQUEST:', JSON.stringify(ctx.request.body.query))
  if (ctx.request.body.image) {
    const base64Only = ctx.request.body.image.base64.replace(/data:image\/(jpeg|png)\;base64\,/, '')
    const ocrParams = {
      ...params,
      Document: {
        Bytes: Buffer.from(base64Only, "base64")
      }
    }
    const ocrResponse = await analyze_document_text(ocrParams, ctx.request.body.query,  ctx.request.body.confidenceMinimum);
    ctx.body = ocrResponse;
  } else {
    ctx.status = 400;
    ctx.body = { message: "Please ensure your request includes a confidenceMinimum, query and image: { base64 } keys." };
  }
  // respond with simple echo for now
  ctx.set('Content-Type', `application/json`);
  ctx.set('Access-Control-Allow-Origin', `*`);
});

app.listen(3100);
console.log('Backend listening at http://localhost:3100\n');